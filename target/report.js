$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/inserir_conta.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Login inválido",
  "description": "\r\nComo um usuário \r\nGostaria de logar no sistema com login inváldo\r\nPara que eu possa verificar a mensagem de retorno",
  "id": "login-inválido",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 2,
      "name": "@funcionais"
    }
  ]
});
formatter.scenarioOutline({
  "line": 12,
  "name": "Deve validar regras do login invalido",
  "description": "",
  "id": "login-inválido;deve-validar-regras-do-login-invalido",
  "type": "scenario_outline",
  "keyword": "Esquema do Cenário"
});
formatter.step({
  "line": 13,
  "name": "clico no botão",
  "keyword": "Quando "
});
formatter.step({
  "line": 14,
  "name": "informo o numero \"\u003cnumero\u003e\"",
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "recebo a mensagem \"\u003cmensagem\u003e\"",
  "keyword": "Então "
});
formatter.examples({
  "line": 17,
  "name": "",
  "description": "",
  "id": "login-inválido;deve-validar-regras-do-login-invalido;",
  "rows": [
    {
      "cells": [
        "numero",
        "mensagem"
      ],
      "line": 18,
      "id": "login-inválido;deve-validar-regras-do-login-invalido;;1"
    },
    {
      "cells": [
        "999",
        "Digite um e-mail ou número de telefone válido"
      ],
      "line": 19,
      "id": "login-inválido;deve-validar-regras-do-login-invalido;;2"
    }
  ],
  "keyword": "Exemplos"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 10,
  "name": "que desejo gerar dados ficticios",
  "keyword": "Dado "
});
formatter.match({
  "location": "InserirContasSteps.queDesejoGerarDadosFicticios()"
});
formatter.result({
  "duration": 97115707600,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Deve validar regras do login invalido",
  "description": "",
  "id": "login-inválido;deve-validar-regras-do-login-invalido;;2",
  "type": "scenario",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "line": 2,
      "name": "@funcionais"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "clico no botão",
  "keyword": "Quando "
});
formatter.step({
  "line": 14,
  "name": "informo o numero \"999\"",
  "matchedColumns": [
    0
  ],
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "recebo a mensagem \"Digite um e-mail ou número de telefone válido\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Então "
});
formatter.match({
  "location": "InserirContasSteps.clicoNoBotão()"
});
formatter.result({
  "duration": 5899128000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "999",
      "offset": 18
    }
  ],
  "location": "InserirContasSteps.informoUmNumeroInvalido(String)"
});
formatter.result({
  "duration": 210811700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Digite um e-mail ou número de telefone válido",
      "offset": 19
    }
  ],
  "location": "InserirContasSteps.receboAMensagem(String)"
});
formatter.result({
  "duration": 1016626500,
  "status": "passed"
});
});