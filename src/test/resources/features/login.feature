#language: pt
@funcionais
Funcionalidade: Login inválido

	Como um usuário 
	Gostaria de logar no sistema com login inváldo
	Para que eu possa verificar a mensagem de retorno
	
Contexto:
	Dado que desejo gerar dados ficticios
	
Esquema do Cenário: Deve validar regras do login invalido
	Quando clico no botão 
	E informo o numero "<numero>"
	Então recebo a mensagem "<mensagem>"
	
Exemplos:
   |numero | mensagem 													 								| 
   |999    | Digite um e-mail ou número de telefone válido 			|