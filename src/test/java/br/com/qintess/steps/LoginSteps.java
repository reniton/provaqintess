package br.com.qintess.steps;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import org.openqa.selenium.By;

import com.codeborne.selenide.Configuration;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class LoginSteps {

	@Dado("^que desejo gerar dados ficticios$")
	public void queDesejoGerarDadosFicticios() throws Throwable {
		Configuration.browser = "Chrome";
		open("https://www.fakenamegenerator.com/");
	}

	@Quando("clico no botão$")
	public void clicoNoBotão() throws Throwable {
		$(".content").shouldBe(text(
				"Logged in users can view full social security numbers and can save their fake names to use later."));
		$(By.xpath("//img[@alt='Log in using Google']")).click();
	}

	@Quando("^informo o numero \"([^\"]*)\"$")
	public void informoUmNumeroInvalido(String numero) throws Throwable {
		$("#identifierId").sendKeys(numero);
		$("#identifierId").pressEnter();
	}

	@Entao("^recebo a mensagem \"([^\"]*)\"$")
	public void receboAMensagem(String mensagem) throws Throwable {
		$(".o6cuMc").shouldHave(text(mensagem));
	}

}
